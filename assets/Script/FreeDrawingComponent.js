//
var FreeDrawingComponent = cc.Class({
    extends: cc.Component,
    properties: {
        render: {
            default         : null,
            type            : cc.Graphics
        }
    },
    onLoad:function() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchEnd, this);
    },
    update:function(dt) {
    },
    //
    onTouchStart:function(event) {
        var location = this.node.convertToNodeSpaceAR(event.getLocation());
        this._vertexArray = [];
        this.render.clear();
        this.render.moveTo(location.x, location.y);
        this._vertexArray.push(location);
        this.node.emit('FreeDrawingComponent_onTouchStart', this._vertexArray);
    },
    onTouchMove:function(event) {
        var location = this.node.convertToNodeSpaceAR(event.getLocation());
        var prePosition = this._vertexArray[this._vertexArray.length - 1];
        if (prePosition.sub(location).mag() < 10) {
            return;
        }
        this.render.lineTo(location.x, location.y);
        this.render.stroke();
        this._vertexArray.push(location);
        this.node.emit('FreeDrawingComponent_onTouchMove', this._vertexArray);
    },
    onTouchEnd:function(event) {
        var location = event.getLocation();
        location = this.node.convertToNodeSpaceAR(location);
        this.render.lineTo(location.x, location.y);
        //
        this.node.emit('FreeDrawingComponent_onTouchEnd', this._vertexArray);
    }
});
